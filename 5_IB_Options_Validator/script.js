// DOM element variables
let inputMerge = document.getElementById('inputMerge');
let inputValidate = document.getElementById('inputValidate');
let inputGroups = document.getElementById('inputGroups');
let result = document.getElementById('result');
let errField = document.getElementById('errField');
let onlyShowInvalid = document.getElementById('onlyShowInvalid');
let showFullName = document.getElementById('showFullName');

// Global variables
let groupMappings = {};
let subjectNames = {};
let outputCSV = [];
const dummyGroup = "8";

let validCombos = new Set();
validCombos.add("1,2,3,4,5,6");
validCombos.add("1,2,2,3,4,5");
validCombos.add("1,2,3,3,4,5");
validCombos.add("1,2,3,4,4,5");
validCombos.add("1,1,3,4,5,6");
validCombos.add("1,1,3,3,4,5");
validCombos.add("1,1,3,4,4,5");

inputGroups.addEventListener('change', function() {
  // Check FileReader compatibility
  if (window.FileReader) {
    let reader = new FileReader();
    reader.readAsText(this.files[0]);
    reader.onload = function (myEvent) {
      let csv = myEvent.target.result;
      let groupsArr =  $.csv.toArrays(csv);

      for (let i = 1; i < groupsArr.length; ++i) {
        let row = groupsArr[i];
        let [mkey, fullname, , grp] = row;
        groupMappings[mkey] = grp;
        subjectNames[mkey] = fullname;
      }
    }

    reader.onerror = displayError("Error with FileReader.");

    // Enable other controls
    inputValidate.disabled = false;
    onlyShowInvalid.disabled = false;
    showFullName.disabled = false;
  } else {
    displayError("FileReader not compatible with your browser.");
  }

});

function displayError(msg) {
  errField.innerHTML = `<div class="alert alert-danger">`
                      + `<strong>Oh snap!</strong> ${msg}</div>`;
}

function displaySuccess(msg) {
  errField.innerHTML = `<div class="alert alert-success">`
                      + `<strong>Well done!</strong> ${msg}</div>`;
}

function validator(file) {
  // Check FileReader compatibility
  if (window.FileReader) {
    let reader = new FileReader();
    reader.readAsText(file);
    reader.onload = function (myEvent) {
      let csv = myEvent.target.result;
      let dataArr =  $.csv.toArrays(csv);

      let allData = {};
      outputCSV = [];
      let maxOptions = 0;

      for (let i = 1; i < dataArr.length; ++i) {
        let row = dataArr[i];
        let [stkey, surname, prefname, tutor, mkey] = row;
        let lvl = mkey.substring(4, 5);

        if (allData.hasOwnProperty(stkey)) {
          // Student already exists
          let info = allData[stkey];

          info.subjects.push(mkey);
          maxOptions = Math.max(maxOptions, info.subjects.length);

          if (lvl == 6) {
            info.higher.push(mkey);
          } else if (lvl == 5) {
            info.standard.push(mkey);
          }

          if (groupMappings[mkey] == "") {
            // No groups (SEN) - push dummy text
            info.groups.forEach(arr => arr.push(dummyGroup));
          } else if (mkey === "12ES5") {
            // Make two copies of existing groups, simulate group 3/4
            let clone = info.groups[0].slice();
            info.groups[0].push(String(3));
            clone.push(String(4));
            info.groups.push(clone);
          } else {
            info.groups.forEach(arr => arr.push(groupMappings[mkey]));
          }

          // Sort group data in ascending order
          for (let i = 0; i < info.groups.length; ++i) {
            info.groups[i] = info.groups[i].sort();
          }

          allData[stkey] = info;
        } else {
          // Student doesn't exist - add
          let info = {
            "surname": surname,
            "prefname": prefname,
            "tutor": tutor,
            "subjects": [mkey],
            "higher": [],
            "standard": [],
            "groups": []
          };

          maxOptions = Math.max(maxOptions, info.subjects.length);

          if (groupMappings[mkey] == "") {
            // No group (SEN) - push dummy text
            info.groups.push([dummyGroup])
          } else if (mkey === "12ES5") {
            // ESS edge case
            info.groups.push([String(3)]);
            info.groups.push([String(4)]);
          } else {
            info.groups.push([groupMappings[mkey]]);
          }

          if (lvl == 6) {
            // Push to HL
            info.higher.push(mkey);
          } else if (lvl == 5) {
            // Push to SL
            info.standard.push(mkey);
          }

          // Push to grand mapping
          allData[stkey] = info;
        }
      }

      // Display results
      let output = "";

      // Header
      output += `<tr>`
              + `<th>STKEY</th>`
              + `<th>Surname</th>`
              + `<th>PrefName</th>`
              + `<th>RollGroup</th>`
              + `<th>Valid?</th>`
              + `<th>Groups</th>`;

      outputCSV.push(["STKEY", "SURNAME", "PREF_NAME", "ROLL_GROUP", "VALID?", "GROUPS"]);

      for (let i = 1; i <= maxOptions; ++i) {
        output += `<th>Opt ${i}</th>`;
        outputCSV[0].push(`Opt ${i}`);
      }

      output += `</tr>`;

      let showInvalid = onlyShowInvalid.checked;
      let fullNames = showFullName.checked;

      let stkeys = Object.keys(allData);
      for (let i = 0; i < stkeys.length; ++i) {
        let stkey = stkeys[i];
        let info = allData[stkey];

        let groups = info.groups;

        // Check combos - only look at first 6 subjects
        let valid = groups.reduce((acc, group) => {
          return acc || validCombos.has(group.slice(0,6).toString());
        }, false);

        // Check HL/SL
        valid = valid && ((info.higher.length == 3 & info.standard.length == 3) || (info.higher.length == 4 & info.standard.length == 2 && info.higher.indexOf("12MD6") > -1));

        if (!valid || !showInvalid) {
          output += valid  ? `<tr>` : `<tr class="danger">`;
          output += `<td>${stkey}</td>`;
          output += `<td>${info.surname}</td>`;
          output += `<td>${info.prefname}</td>`;
          output += `<td>${info.tutor}</td>`;
          output += `<td>${valid ? "Yes" : "No"}</td>`;
          output += `<td>`;

          outputCSV.push([stkey, info.surname, info.prefname, info.tutor, valid ? "Yes" : "No"]);

          let summariseGroups = "";
          for (let j = 0; j < groups.length; ++j) {
            output += groups[j].toString();
            summariseGroups += groups[j].join("-");
            if (j + 1 < groups.length) {
              output += `<br />`;
              summariseGroups += " or ";
            }
          }

          outputCSV[outputCSV.length - 1].push(summariseGroups);
          output += `</td>`;
          for (let j = 0; j < info.subjects.length; ++j) {
            if (fullNames) {
              // Show subject full names
              output += `<td>${subjectNames[info.subjects[j]]}</td>`;
              outputCSV[outputCSV.length - 1].push(subjectNames[info.subjects[j]]);
            } else {
              // Just show MKEY
              output += `<td>${info.subjects[j]}</td>`;
              outputCSV[outputCSV.length - 1].push(info.subjects[j]);
            }

          }

          for (let j = info.subjects.length; j < maxOptions; ++j) {
            output += `<td> </td>`;
            outputCSV[outputCSV.length - 1].push("");
          }
          output += `</tr>`;
        }

      }

      // Display output
      result.innerHTML = output;

      // Success message
      displaySuccess('Options were successfully validated. <strong><a id="downloadCSV" href="javascript:void(0);" onclick="downloadCSV()">Download the CSV here.</a></strong>');
    }

  } else {
    displayError("FileReader not compatible with your browser.");
  }
}

onlyShowInvalid.addEventListener('change', function() {
  validator(inputValidate.files[0]);
});

showFullName.addEventListener('change', function() {
  validator(inputValidate.files[0]);
});

inputValidate.addEventListener('change', function() {
  validator(this.files[0]);
});

function downloadCSV() {
  let csvEnv = "data:text/csv;charset=utf-8,";
  console.log(outputCSV);
  outputCSV.forEach((arr, index) => {
    let dataStr = arr.join(",");
    csvEnv += (index < outputCSV.length) ? `${dataStr}\n` : dataStr;
  });

  let encodedURI = encodeURI(csvEnv);
  let downloadLink = document.createElement("a");
  downloadLink.setAttribute("href", encodedURI);
  let name = prompt('Enter the name for the exported CSV file:');
  if (name.indexOf(".") > -1) {
    // Remove any extension - just parse name
    name = name.substring(0, name.indexOf("."));
  }

  downloadLink.setAttribute("download", `${name}.csv`);
  document.body.appendChild(downloadLink);
  downloadLink.click();
  alert(`${name}.csv has been successfully exported to your default Downloads folder.`);
}
