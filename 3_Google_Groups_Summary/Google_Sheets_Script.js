// Global variables
var currFile = SpreadsheetApp.getActiveSpreadsheet();

function getGroups() {
  // Clean up first
  resetSheets();
  
  // Initialise variables
  var pgtoken, pg;
  
  // Get users
  var mapIdToName = getUsers();
  
  do {
    // Go to next group page, if applicable
    pg = AdminDirectory.Groups.list({
      domain: 'stconline.edu.hk',
      maxResult: 250,
      pageToken: pgtoken
    });
    
    // Get groups from requests
    var groups = pg.groups;
    
    // Null check
    if (groups) {
      // Loop through each group
      for (var i = 0; i < groups.length; ++i) {
        var rows = [];
        
        // Parse group from array
        var group = groups[i];
        
        // Get group members by email
        var members = AdminDirectory.Members.list(group.email).members;
        
        // Check if there are members
        if (members) {
          Logger.log("Members: " + String(members.length));
          // Loop through each member
          for (var j = 0; j < members.length; ++j) {
            // Parse member from array
            var member = members[j];
            
            // Lookup name
            var name = mapIdToName[member.id];
            
            // Error handling if name does not exist
            if (!name) {
              name = "--";
            }
            
            // Add to row
            var row = [name, member.email];
            rows.push(row);
          }
        }
       
        // Create sheet for group
        var newSheet;
        try {
          newSheet = currFile.insertSheet(group.name);
        } catch (err) {
          // Error usually due to duplicate name
          newSheet = currFile.insertSheet(group.name + "-copy");
        }
        
        // Error handling for new sheets
        try {
          newSheet = currFile.getSheetByName(group.name);
          
          // Modify row/columns
          var maxRows = newSheet.getMaxRows();
          var maxCols = newSheet.getMaxColumns();
          newSheet.deleteRows(rows.length + 3, maxRows - rows.length - 2);
          newSheet.deleteColumns(3, maxCols - 2);
          
          // Setup title
          var titleRow = newSheet.getRange("A1:B1").merge();
          titleRow.setFontWeight("bold");
          titleRow.setValue(String(group.name) + " - " + String(group.email))
          titleRow.setHorizontalAlignment("center");
          
          // Setup header
          newSheet.getRange("A2").setValue("Name");
          newSheet.getRange("B2").setValue("Email");
          
          // Put values
          if (rows.length > 0) {
            newSheet.getRange(3, 1, rows.length, 2).setValues(rows);
          }
          
          // Resize columns
          newSheet.autoResizeColumn(1);
          newSheet.autoResizeColumn(2);
          
        } catch (err) {
          // Log error 
          Logger.log(err);
        }
        
      }
    }
  
    pgToken = pg.nextPageToken;
  } while (pgtoken);
}

/* 	
POPULATE USER DICTIONARY
*/
function getUsers() {
  // Initialise variables
  var mapIdToName = {};
  var pageToken, page;
  
  do {
    // Go to next user page, if applicable
    page = AdminDirectory.Users.list({
      domain: 'stconline.edu.hk',
      maxResult: 250,
      pageToken: pageToken
    });
    
    // Get users from request
    var users = page.users;
    
    // Null check
    if (users) {
      // Loop through each user
      for (var i = 0; i < users.length; ++i) {
        // Error handling with try-catch block
        try {
          // Parse user ID and name from array
          var user = users[i];
          
          // Add to dictionary
          mapIdToName[user.id] = user.name.fullName;
        } catch (err) {
          // Log error
          Logger.log(err);
        }
      }
    }
    
    pageToken = page.nextPageToken;
  } while (pageToken);
  
  return mapIdToName;
}

function resetSheets() {
  var allSheets = currFile.getSheets();
  
  for (var i = 0; i < allSheets.length; ++i) {
    if (allSheets[i].getName() !== "Sheet1") {
      currFile.deleteSheet(allSheets[i]);
    }
  }
} 
