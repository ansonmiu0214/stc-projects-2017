SELECT ST.STKEY, (ST.SURNAME + ', ' + ST.PREF_NAME) AS FULLNAME, ST.ROLL_GROUP, STMA.MKEY, SU.FULLNAME
FROM STMA
INNER JOIN ST ON ST.STKEY = STMA.SKEY
INNER JOIN SU ON STMA.MKEY = SU.SUKEY
WHERE ST.STATUS = 'FULL' AND ST.SCHOOL_YEAR = 'Y12' AND STMA.TTPERIOD = '2017' AND STMA.MKEY NOT LIKE '12TG%' AND STMA.MKEY NOT IN ('12CZ1', '12TK7', '12GT1', '12PE1')
ORDER BY ST.ROLL_GROUP ASC, ST.STKEY ASC